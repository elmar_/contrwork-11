import React from 'react';
import {TextField} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";

const FormElement = ({required, name, label, value, onChange, error, type, autoComplete, ...props}) => {
  return (
    <Grid item xs>
      <TextField
        label={label}
        type={type}
        value={value}
        onChange={onChange}
        name={name}
        error={Boolean(error)}
        helperText={error}
        required={required}
        autoComplete={autoComplete}
        {...props}
      />
    </Grid>
  );
};

export default FormElement;