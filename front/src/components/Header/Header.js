import React from 'react';
import {AppBar, Container, createStyles, Grid, makeStyles, Toolbar, Typography} from "@material-ui/core";
import {useSelector} from "react-redux";
import {Link} from "react-router-dom";
import UserMenu from "./UserMenu/UserMenu";
import Anonymous from "./Anonymous/Anonymous";

const useStyles = makeStyles(theme =>
  createStyles({
    title: {
      color: 'inherit',
      textDecoration: 'none'
    },
    appBar: {
      marginBottom: 30
    }
  }),
);

const Header = () => {
  const classes = useStyles();
  const user = useSelector(state => state.users.user);

  return (
    <AppBar position="static" className={classes.appBar}>
      <Container>
        <Toolbar>
          <Grid container justify='space-between' direction='row'>
            <Grid item>
              <Typography variant="h6" className={classes.title} component={Link} to='/'>
                All items
              </Typography>
            </Grid>
            <Grid item>
              {user ? (
                <UserMenu user={user} />
              ) : (
                <Anonymous/>
              )}
            </Grid>
          </Grid>
        </Toolbar>
      </Container>
    </AppBar>
  );
};

export default Header;