import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import {logoutUser} from "../../../store/actions/usersActions";
import Button from "@material-ui/core/Button";
import {Menu, MenuItem} from "@material-ui/core";
import {historyPushStraight} from "../../../store/actions/historyActions";

const UserMenu = ({user}) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const dispatch = useDispatch();

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const onClick = () => {
    historyPushStraight('/createItem');
    setAnchorEl(null);
  };

  const logout = () => {
    dispatch(logoutUser());
    setAnchorEl(null);
  };

  return (
    <>
      <Button
        onClick={handleClick}
        color='inherit'
      >
        Hello, {user.username}!
      </Button>
      <Menu
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem onClick={onClick}>Create item</MenuItem>
        <MenuItem onClick={logout}>Logout</MenuItem>
      </Menu>
    </>
  );
};

export default UserMenu;