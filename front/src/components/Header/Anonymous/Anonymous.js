import React from 'react';
import {Button} from "@material-ui/core";
import {Link} from "react-router-dom";

const Anonymous = () => {
  return (
    <>
      <Button color="inherit" component={Link} to='/login'>Login</Button>
      <Button color="inherit" component={Link} to='/register'>Register</Button>
    </>
  );
};

export default Anonymous;