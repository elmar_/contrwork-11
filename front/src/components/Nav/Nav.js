import React from 'react';
import {Divider, List, ListItem, ListItemText} from "@material-ui/core";
import {Link} from "react-router-dom";
import Grid from "@material-ui/core/Grid";

const Nav = () => {
  return (
    <Grid item xs={3}>
      <List component="nav" aria-label="main mailbox folders">
        <ListItem button component={Link} to='/'>
          <ListItemText primary="All items"/>
        </ListItem>
        <Divider />
        <ListItem button component={Link} to='/books'>
          <ListItemText primary="Books" />
        </ListItem>
        <Divider />
        <ListItem button component={Link} to='/phones'>
          <ListItemText primary="Phones" />
        </ListItem>
        <Divider />
        <ListItem button component={Link} to='/doors'>
          <ListItemText primary="Doors" />
        </ListItem>
        <Divider />
      </List>
    </Grid>
  );
};

export default Nav;