import React from 'react';
import {Card, CardActionArea, CardContent, CardMedia, makeStyles, Typography} from "@material-ui/core";
import noImage from "../../assets/no-image.jpg";
import {apiURL} from "../../config";

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    minWidth: 300
  },
  media: {
    height: 200,
  },
});

const Carding = ({onclick, title, image, description, category, user}) => {
  const classes = useStyles();

  let cardImage = noImage;

  if (image) {
    cardImage = apiURL + '/' + image;
  }

  return (
    <>
      <Card className={classes.root} onClick={onclick}>
        <CardActionArea>
          <CardMedia
            className={classes.media}
            image={cardImage}
            title={title}
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              {title}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              description: {description}<br/>
              Category: {category}<br/>
              Owner: {user?.displayName}<br/>
              Phone: {user?.phone}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </>
  );
};

export default Carding;