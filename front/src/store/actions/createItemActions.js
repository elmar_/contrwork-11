import axiosApi from "../../axiosApi";
import {historyPush} from "./historyActions";

export const CREATE_ITEM_REQUEST = 'CREATE_ITEM_REQUEST';
export const CREATE_ITEM_SUCCESS = 'CREATE_ITEM_SUCCESS';
export const CREATE_ITEM_ERROR = 'CREATE_ITEM_ERROR';

const createItemRequest = () => ({type: CREATE_ITEM_REQUEST});
const createItemSuccess = () => ({type: CREATE_ITEM_SUCCESS});
const createItemError = error => ({type: CREATE_ITEM_ERROR, error});

export const createItem = (item, token) => {
  return async dispatch => {
    try {
      dispatch(createItemRequest());
      const headers = {'Authorization': token};
      await axiosApi.post('/items', item, {headers});
      dispatch(createItemSuccess());
      dispatch(historyPush('/'));
    } catch (e) {
      if (e.response && e.response.data) {
        dispatch(createItemError(e.response.data));
      } else {
        dispatch(createItemError({global: 'No internet'}));
      }
    }
  };
};