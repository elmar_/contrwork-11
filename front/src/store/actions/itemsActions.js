import axiosApi from "../../axiosApi";

export const FETCH_ITEMS_REQUEST = 'FETCH_ITEMS_REQUEST';
export const FETCH_ITEMS_SUCCESS = 'FETCH_ITEMS_SUCCESS';
export const FETCH_ITEMS_ERROR = 'FETCH_ITEMS_ERROR';

const fetchItemsRequest = () => ({type: FETCH_ITEMS_REQUEST});
const fetchItemsSuccess = items => ({type: FETCH_ITEMS_SUCCESS, items});
const fetchItemsError = error => ({type: FETCH_ITEMS_ERROR, error});

export const fetchItems = category => {
  return async dispatch => {
    try {
      dispatch(fetchItemsRequest());
      const response = await axiosApi.get('/items', {params: category});
      dispatch(fetchItemsSuccess(response.data));
    } catch (e) {
      if (e.response && e.response.data) {
        dispatch(fetchItemsError(e.response.data));
      } else {
        dispatch(fetchItemsError({global: 'No internet'}));
      }
    }
  }
}