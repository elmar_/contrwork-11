import {CREATE_ITEM_ERROR, CREATE_ITEM_REQUEST, CREATE_ITEM_SUCCESS} from "../actions/createItemActions";

const initialState = {
  loading: false,
  error: null,
};

const createItemReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_ITEM_REQUEST:
      return {...state, loading: true};
    case CREATE_ITEM_SUCCESS:
      return {...state, loading: false, error: false}
    case CREATE_ITEM_ERROR:
      return {...state, loading: false, error: action.error};
    default:
      return state;
  }
};

export default createItemReducer;