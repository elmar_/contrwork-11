import {FETCH_ITEMS_ERROR, FETCH_ITEMS_REQUEST, FETCH_ITEMS_SUCCESS} from "../actions/itemsActions";

const initialState = {
  loading: false,
  error: null,
  items: []
};

const itemsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ITEMS_REQUEST:
      return {...state, loading: true};
    case FETCH_ITEMS_SUCCESS:
      return {...state, loading: false, items: action.items, error: null};
    case FETCH_ITEMS_ERROR:
      return {...state, loading: false, error: action.error}
    default:
      return state;
  }
};

export default itemsReducer;