import React from 'react';
import {Route, Switch} from 'react-router-dom';
import {Container, createStyles, CssBaseline, makeStyles} from "@material-ui/core";
import Header from "./components/Header/Header";
import Items from "./containers/Items/Items";
import Register from "./containers/User/Register/Register";
import Login from "./containers/User/Login/Login";
import Grid from "@material-ui/core/Grid";
import Nav from "./components/Nav/Nav";
import CreateItem from "./containers/CreateItem/CreateItem";
import Phones from "./containers/Categories/Phones";
import Doors from "./containers/Categories/Doors";
import Books from "./containers/Categories/Books";

const useStyles = makeStyles(theme =>
  createStyles({
    content: {
      padding: theme.spacing(0, 3)
    }
  }),
);

const App = () => {
  const classes = useStyles();

  return (
    <>
      <CssBaseline/>
      <header>
        <Header />
      </header>
      <main>
        <Container>
          <Grid container>
            <Nav />
            <Grid item xs className={classes.content}>
              <Switch>
                <Route path='/' exact component={Items} />
                <Route path='/login' component={Login} />
                <Route path='/register' component={Register} />
                <Route path='/createItem' component={CreateItem} />
                <Route path='/books' component={Books} />
                <Route path='/doors' component={Doors} />
                <Route path='/phones' component={Phones} />
              </Switch>
            </Grid>
          </Grid>
        </Container>
      </main>

    </>
  );
};

export default App;