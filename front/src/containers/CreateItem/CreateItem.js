import React, {useEffect, useState} from 'react';
import {createStyles, InputLabel, makeStyles, MenuItem, Select, Typography} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import FormElement from "../../components/UI/FormElement/FormElement";
import FileInput from "../../components/UI/FileInput/FileInput";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import {createItem} from "../../store/actions/createItemActions";
import {useDispatch, useSelector} from "react-redux";
import {historyPushStraight} from "../../store/actions/historyActions";
import {Alert, AlertTitle} from "@material-ui/lab";

const useStyles = makeStyles(theme =>
  createStyles({
    formBlock: {
      padding: theme.spacing(0, 10),
      margin: theme.spacing(4, 0, 0)
    },
    button: {
      display: 'block',
      marginTop: theme.spacing(2),
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    submit: {
      margin: theme.spacing(3, 0, 2)
    },
  }),
);


const CreateItem = () => {

  const classes = useStyles();
  const [item, setItem] = useState({
    category: '',
    title: '',
    description: '',
    image: ''
  });
  const [open, setOpen] = React.useState(false);
  const dispatch = useDispatch();
  const user = useSelector(state => state.users.user);
  const loading = useSelector(state => state.createItem.loading);
  const error = useSelector(state => state.createItem.error);

  if (!user) {
    historyPushStraight('/register');
  }

  const inputChangeHandler = e => {
    const {name, value} = e.target;

    setItem(prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  const fileChangeHandler = e => {
    const name = e.target.name;
    const file = e.target.files[0];

    setItem(prevState => ({
      ...prevState,
      [name]: file
    }));
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  const submitFormHandler = e => {
    e.preventDefault();

    const formData = new FormData();

    Object.keys(item).forEach(key => {
      formData.append(key, item[key]);
    });

    dispatch(createItem(formData, user.token));
  };

  return (
    <>
      <Typography variant='h4'>
        Create item
      </Typography>
      <form onSubmit={submitFormHandler}>
        <Grid container direction='column' spacing={3} className={classes.formBlock}>
          {error && (
            <Alert severity="error">
              <AlertTitle>Error</AlertTitle>
              Something go wrong, check internet and try again
            </Alert>
          )}
          <FormElement
            label="Title"
            onChange={inputChangeHandler}
            name='title'
            value={item.title}
            type='text'
            required
            fullWidth
          />
          <FormElement
            label="Description"
            onChange={inputChangeHandler}
            name='description'
            value={item.description}
            type='text'
            required
            fullWidth
            multiline
            rows={3}
          />
          <Grid item xs>
            <FileInput
              name="image"
              label="Image"
              onChange={fileChangeHandler}
            />
          </Grid>

          <Grid item xs>
            <InputLabel id="demo-controlled-open-select-label">Category</InputLabel>
            <Select
              labelId="Category"
              id="demo-controlled-open-select"
              open={open}
              onClose={handleClose}
              onOpen={handleOpen}
              name='category'
              value={item.category}
              onChange={inputChangeHandler}
              fullWidth
              required
            >
              <MenuItem value='book'>Book</MenuItem>
              <MenuItem value='phone'>Phone</MenuItem>
              <MenuItem value='door'>Door</MenuItem>
            </Select>
          </Grid>
          <Grid item xs>
            <ButtonWithProgress
              type='submit'
              fullWidth
              variant='contained'
              color='primary'
              className={classes.submit}
              loading={loading}
              disabled={loading}
            >
              Create
            </ButtonWithProgress>
          </Grid>
        </Grid>
      </form>
    </>
  );
};

export default CreateItem;