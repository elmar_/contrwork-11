import React, {useEffect} from 'react';
import {Typography} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import {fetchItems} from "../../store/actions/itemsActions";
import {useDispatch, useSelector} from "react-redux";
import {Alert, AlertTitle} from "@material-ui/lab";
import Spinner from "../../components/UI/Spinner/Spinner";
import Carding from "../../components/Carding/Carding";

const Doors = () => {
  const dispatch = useDispatch();
  const {loading, error, items} = useSelector(state => state.items);

  useEffect(() => {
    dispatch(fetchItems({category: 'door'}));
  }, [dispatch]);

  let content = (
    items.map(item => (
      <Grid item key={item._id}>
        <Carding image={item.image} description={item.description} title={item.title} category={item.category} user={item.user}/>
      </Grid>
    ))
  );

  if (loading) content = <Spinner />


  return (
    <>
      <Typography variant='h4'>
        Doors
      </Typography>
      {error && (
        <Alert severity="error">
          <AlertTitle>Error</AlertTitle>
          Something go wrong, check internet and try again
        </Alert>
      )}
      <Grid container spacing={2} style={{marginTop: 20}}>
        {content}
      </Grid>
    </>
  );
};

export default Doors;