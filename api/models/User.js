const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const {nanoid} = require("nanoid");

const SALT_WORK_FACTOR = 10;

const UserSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    unique: true,
    validate: {
      validator: async function(value)  {
        if (this.isModified('username')) {
          const user = await User.findOne({username: value});
          if (user) return false;
        }

        return true;
      },
      message: 'This user is already registered'
    }
  },
  password: {
    type: String,
    required: true
  },
  token: {
    type: String,
    required: true
  },
  displayName: {
    type: String,
    required: true,
    unique: true,
    validate: {
      validator: async function(value)  {
        if (this.isModified('displayName')) {
          const user = await User.findOne({displayName: value});
          if (user) return false;
        }

        return true;
      },
      message: 'This display name is already registered'
    }
  },
  phone: {
    type: String,
    required: true,
    unique: true,
    validate: {
      validator: async function(value)  {
        if (this.isModified('phone')) {
          const user = await User.findOne({phone: value});
          if (user) return false;
        }

        return true;
      },
      message: 'This phone is already registered'
    }
  }
});

UserSchema.pre('save', async function (next) {
  if (!this.isModified('password')) return next();

  const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
  const hash = await bcrypt.hash(this.password, salt);

  this.password = hash;

  next();
});

UserSchema.set('toJSON', {
  transform: (doc, ret, options) => {
    delete ret.password;
    return ret;
  }
});

UserSchema.methods.checkPassword = function (password) {
  return bcrypt.compare(password, this.password);
};

UserSchema.methods.generateToken = function () {
  this.token = nanoid();
};

const User = mongoose.model('User', UserSchema);

module.exports = User;