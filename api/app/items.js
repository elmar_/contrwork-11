const express = require('express');
const auth = require("../middleware/auth");
const config = require("../config");
const {nanoid} = require("nanoid");
const path = require('path');
const multer = require("multer");
const Item = require("../models/Item");

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  },
});

const upload = multer({storage});

router.post('/', auth, upload.single('image'), async (req, res) => {
  try {
    const itemData = req.body;

    if (req.file) {
      itemData.image = 'uploads/' + req.file.filename;
    }

    itemData.user = req.user._id;

    const item = new Item(itemData);
    await item.save();
    return res.send(item);

  } catch (error) {
    return res.status(400).send(error);
  }
});

router.get('/', async (req, res) => {
  const category = req.query;
  const items = await Item.find(category).populate('user');

  return res.send(items);
});

// router.delete('/session', async (req, res) => {
// });

module.exports = router;